<?php

Auth::routes();

//movies
Route::get('/home', 'CinemaController@index');

Route::get('/index', 'CinemaController@index');

Route::get('/movie/{id}', 'CinemaController@showMovie');


//tickets
Route::get('/ticket/{id}' , 'CinemaController@getTicket');

Route::post('/buy' , 'CinemaController@buyTicket')->name('buyTicket');

Route::post('/sendTickets', 'CinemaController@createPDF')->name('sendTickets');

//seats
Route::get('/seat', 'CinemaController@chooseSeat');

Route::post('/seat','CinemaController@confirmSeat');




//Route::get('/', 'HomeController@index');
