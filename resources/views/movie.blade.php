@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$movie->name}} - {{$movie->year}}</div>
                    <img src="" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p>{{$movie->overview}}</p>
                    </div>
                    
                	<div class="card-body mt-3 ml-0 text-center" >
                       <a id="buy" class="btn btn-primary" href="/ticket/{{$movie->id}}">Buy Ticket</a>
                       <a id="cancel"  class="btn btn-warning ml-3 text-light" href="/home">Return to Index</a>
                    </div>
                
                </div>
                
            </div>
        </div>
    </div>
@endsection