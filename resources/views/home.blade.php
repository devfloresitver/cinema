@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
        	@foreach($movies as $movie)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">{{$movie->name}} ({{$movie->year}})</div>
                    <a href="movie/{{$movie->id}}" title=""><img src="https://placeholder.pics/svg/175/DEDEDE/555555/example" class="card-img-top" alt="..." max-width="175px"></a>
                    <div class="card-body">
                        <p>{{$movie->overview}}</p>
                    </div>
                </div>
            </div>
             @endforeach
        </div>
        {{$movies->links()}}
    </div>
@endsection
