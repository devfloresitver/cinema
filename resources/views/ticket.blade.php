@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
                <div class="card">
                  <div class="card-header bg-light text-dark">{{$ticket->salon}}{{$ticket->seat}}</div>
                  <div class="card-body text-center">
                    <img src="https://placeholder.pics/svg/175/DEDEDE/555555/example" class="card-img-top" alt="..." max-width="175px">
                  <p class="card-text ">Movie: {{$ticket->movies->name}}</p>
                  {{--<p class="card-text">Excerpt: {{$ticket->movies->overview}}</p>--}}
                  <p class="card-text">Salon: {{$ticket->salon}}</p>
                  <p class="card-text">Seat: {{$ticket->seat}}</p>
                  <p class="card-text">Total: ${{$ticket->movies->tariff}}</p>


                  <form action="{{route('buyTicket')}}" method="POST">
                    @csrf()
                    <div class="form-group">
                      <label for="ticket-id"></label>
                      <input type="number" name="ntickets"  class="form-control" step="1" value="1" readonly min ="1" max="10">
                      <input type="hidden" class="form-control" name="movie_id" value="{{$ticket->movies->id}}" required>
                      <input type="hidden" class="form-control" name="ticket_id" value="{{$ticket->id}}" required>                  
                    </div>

                    <div class="card-body  mt-3 ml-0" >
                       <button type="submit" class="btn btn-primary">Confirm</button>
                       <a id="cancel" href="/" class="btn btn-warning ml-3 text-light" >Cancel</a>
                    </div>
                   
                  </form>

                  
                	
                  
                 
                  
                  </div>

                </div>
        		</div>
		</div>
</div>
@endsection