@extends('layouts.app')

@section('content')
	<div class="container jumbotron" id="main">
	@if($total > 0)
		<div class="alert alert-success" role="alert">
  			<h4 class="display-4">Order completed successfully!</h4>
		</div>
		<hr class="my-4">
		<div class="container lead">
			<a href="/home" class="btn btn-link">Go Home.</a>
			<button id="getTickets"class="btn btn-link">Send Tickets to mail</<button>
		</div>
	@else
		<div class="alert alert-danger" role="alert">
		  <h4>There wasn an error processing your order.</h4>
		</div>
	@endif
	</div>

	<script>
	document.getElementById('getTickets').addEventListener('click', function(){
		axios.post('/sendTickets').then((res)=>{
			if(res.data != null){
				const notification = document.createElement('div');
				notification.innerHTML = `<div class="alert alert-success" role="alert">
  			<h2 class="display-2">Tickets sent.</h2>
		</div>`;
				document.getElementById('main').appendChild(notification);
				window.location.href = '/home';
			}
		})
	});
	</script>

@endsection

