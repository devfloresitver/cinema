<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Ticket;
use App\Producer;
use App\Order;
use App\User;
use App\EmailWithAttachments;
use PDF;
use Session;
use Mail;
use Carbon\Carbon;

class CinemaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $movies = Movie::paginate(16);
        return view('home', compact('movies', $movies));
    }

    public function showMovie($id){
        if($this->movieExists($id)){
        $movie = Movie::find($id);
        return view('movie', compact('movie', $movie));
        }else{

        }
    }

    public function getTicket($movie_id){
         //$movie = Movie::find($movie_id);
        if($this->movieExists($movie_id)){
         $ticket = $this->generateTicket($movie_id);
         //dump($movie);
         //dd($movie->tickets()->find($ticket->id));
         //dd($ticket->movies()->find($movie_id));
    	 return view('ticket', compact('ticket', $ticket));
        //return view('ticket', compact('ticket', $ticket));
        }else{

        }
    }

    protected function movieExists($id){
        return $movie = Movie::find($id);
    }

    public function generateTicket($movie_id){
       $ticket = new Ticket();
       $ticket->seat = (string) $this->getRandomSeat();
       $ticket->salon = $this->getRandomSalon();
       $ticket->movie_id = (int) $movie_id;
       $ticket->save();
       //get latest current ticket
       return Ticket::orderBy('id', 'desc')->first();
    }

    protected function getRandomSalon(){
        $salon = "";
        $dict = "ABCDEFGHI";
        $salon=substr($dict,rand(0,strlen($dict)),1);
        return $salon;

    }

    protected function getRandomSeat(){
        $seat = rand(1, 60);
        return $seat;
    }



    public function buyTicket(Request $request){
        $movie_id = $request->movie_id;
        $ticket_id = $request->ticket_id;
        $ntickets = $request->ntickets;
        $success = 0;
        $total = 0;
        //dd($request->all());
        if($movie_id && $ticket_id){
            $success= 1;
            $total = $this->calculateTotal($ntickets, $movie_id);
            Session::put('movie_name', Movie::find($movie_id)->name);

        }
        //dd($success == 1);
        return view('success')->with('total', $total);
    }

    protected function calculateTotal($ntickets, $id){
        $tariff = Movie::find($id)->tariff;
        $total = $tariff * $ntickets;
        $this->registerUserOrder($total);
        return $total;
    }

    protected function registerUserOrder($total){
        $order_total = $total;
        $user_id = auth()->id();
        $newUserOrder = new Order();
        $newUserOrder->total = $order_total;
        $newUserOrder->user_id = $user_id;
        $newUserOrder->save();
        Session::put('order_id',  Order::orderBy('id', 'desc')->first()->id);
        
        return true;
    }

    //email and pdf functions
    public function createPDF(Request $request){
          $data = $this->getUserEmailData($request);  
          $filepath = storage_path('pdf').'\\u'.auth()->id().'.pdf';
          $pdf = \PDF::loadView('pdf.email', $data)->save($filepath);
          $sent = 'error';
          if($this->sendEmail($filepath, $data)){
            $sent = 'success';
          }
          $this->clearSessionVariables();
          //dd($sent);
          //return 'ok';
          return 'ok'; 
    }


    protected function sendEmail($filepath, $orderData){
        $user = User::find(auth()->id());
        $user_email = $user->email;
        $pdf_path = [$filepath];
        $data =  $orderData;
        $view = 'pdf.email';
        $subject = 'Your Tickets are ready';
    Mail::to($user_email)->send(new EmailWithAttachments($data, 
        $subject, $view, $pdf_path, true));
        return true;
    }

    protected function getUserEmailData($request){
        $user = User::find(auth()->id());
        $user_email = $user->email;
        //$pdf_path = $filepath;

        $data = [
                    'username' => $user->name,
                    'order_id' => Session::get('order_id'),
                    'movie_name' => Session::get('movie_name')
                ];

        return $data;
        
    }

    protected function clearSessionVariables(){
        Session::put('order_id','');
        Session::put('movie_name','');

    }

    public function chooseSeat(){
    	return view('seats');
    }

    public function confirmSeat(){
        return 0;
    }

    protected function isChosen($seat_number){
        return true;
    }
}
