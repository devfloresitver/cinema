<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function movies(){
    	return $this->belongsTo(Movie::class, 'movie_id');
    }


}
