<?php

namespace App;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailWithAttachments extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Build an e-mail with attachements.
     *
     * @return void
     */
    protected $aData;
    protected $sSubject;
    protected $sView;
    protected $aAttachedFiles;
    protected $sCC;
    protected $bAttachedFiles;

    public function __construct($aData, $sSubject, $sView, $aAttachedFiles, $bAttachedFiles) {
        $this->aData = $aData;
        $this->sSubject = $sSubject;
        $this->sView = $sView;
        $this->aAttachedFiles = $aAttachedFiles;
        //$this->sCC = $sCC;
        $this->bAttachedFiles = $bAttachedFiles;
    }

    /**
     * build the e-mail
     *
     * @return $this
     */
    public function build() {
        $email = $this//->from('')
                ->subject($this->sSubject)
                //->cc($this->sCC)
                ->view($this->sView)
                ->with($this->aData)
                ->delay(5);

        if($this->bAttachedFiles){
        //attaching files
        foreach ($this->aAttachedFiles as $sFilePath) {
            $email->attach($sFilePath);
            }
        }   
        return $email;
    }

}
