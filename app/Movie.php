<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    public function tickets(){
    	return $this->hasOne(Ticket::class, 'movie_id');
    }

    public function producers(){
    	return $this->belongsTo(Producer::class);
    }
}
