<?php

use Illuminate\Database\Seeder;
use App\Producer;
use App\Movie;
use App\Ticket;

class CinemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Producer::class, 5)->create();
       
        factory(Movie::class, 20)->create();

         factory(Ticket::class, 40)->create();
       
         
    }
}
