<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movie;
use App\Producer;
use App\Ticket;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
	$producers = App\Producer::pluck('id')->toArray();
    return [
        'name' => $faker->name,
        'length' => $faker->randomNumber(4),
        'year' => $faker->randomNumber(4),
        'overview' => $faker->text,
        'producer_id' =>$faker->randomElement($producers),
        'tariff' => $faker->randomNumber(3)
    ];
});
