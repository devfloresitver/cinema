<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ticket;
use App\Movie;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
	$movies = App\Movie::pluck('id')->toArray();
    return [
        'seat' => $faker->randomNumber(4),
        'salon' => $faker->text($maxNbChars = 5),
        'movie_id' => $faker->randomElement($movies)
    ];
});
