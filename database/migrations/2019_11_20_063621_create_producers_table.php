<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('country');
           
            $table->timestamps();
        });


         Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('length');
            $table->unsignedBigInteger('year');
            $table->text('overview');
            $table->unsignedBigInteger('producer_id');
            $table->foreign('producer_id')->references('id')->on('producers')->onDelete('cascade');
            $table->unsignedBigInteger('tariff');
            $table->timestamps();
        });

           Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('seat');
            $table->string('salon');
            $table->timestamps();
            $table->unsignedBigInteger('movie_id');
            $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade');
        });


        Schema::create('orders', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('total');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producers');

        Schema::dropIfExists('movies');

         Schema::dropIfExists('tickets');

         Schema::dropIfExists('orders');
    }
}
